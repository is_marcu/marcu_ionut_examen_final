// instanta       modulul 
var http = require('http');

startServer('Welcome');
// Definitia functiei
function startServer(htmlString) {
    const PORT = 8081;

    function handleRequest(request, response) {
        response.writeHeader(200, {
            "Content-Type" : "text/html"
        });
        response.write(htmlString);
        
        response.end();
    }
    var server = http.createServer(handleRequest);
    server.listen(PORT, function(){
        console.log('Bun Venit', PORT);
    });
}