// instanta       modulul 
var http = require('http');
var fs = require('fs');

fs.readFile('auth.html', function(err, html) {
    
    var htmlString = html.toString('utf-8');


    startServer(htmlString);
});

// Definitia functiei
function startServer(htmlString) {
    const PORT = 8080;

    function handleRequest(request, response) {
        response.writeHeader(200, {
            "Content-Type" : "text/html"
        });
        response.write(htmlString);
        
        response.end();
    }
    var server = http.createServer(handleRequest);
    server.listen(PORT, function(){
        console.log('Bun Venit', PORT);
    });
}