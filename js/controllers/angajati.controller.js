AngajatiController.$inject = ['$scope'];

// $scope - face legatura intre HTML si JavaScript (controller)
function AngajatiController($scope) {
     $scope.angajati = [{nume: 'Ionut', functie : 'programator' }, {nume: 'Sorin', functie : 'tester' }, {nume: 'Cristi', functie : 'tester' }, {nume: 'Vasilica', functie : 'tester' } ];
}